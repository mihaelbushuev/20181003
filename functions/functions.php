<?php // You have to pay for everything. Not money, so time.

function stm_get_data ( $n, $cur_format ) {

	$data = stm_set_data ( $n );

	// Output JSON
	if ( $cur_format == 'json' ) {

		if ( is_array( $data ) ) {
			$res = array( 'error' => false, 'data' => $data );
		} else {
			$res = array( 'error' => $data );
		}

		print json_encode( $res );

	}

	return $data;
}

function stm_set_data ( $n, $data = false, $pdata = false ) {

	if ( ! is_numeric( $n ) ) return 'Dimension not integer';
	if ( $n < 0 ) return 'Dimension not positive';
	if ( $n < 2 ) return 'Dimension less than 2';
	if ( $n > 38 ) return 'Dimension more than 38';

	if ( $data == false ) {
		$data = array( '0' => array( '1' => 1, '0' => 0, ) );
		$pdata = array(
			0 => array( 'r' => 0, 'c' => 0, 'v' => $data[0][0] ),
			1 => array( 'r' => 0, 'c' => 1, 'v' => $data[0][1] ),
			'count' => 2,
			'round' => 0,
		);
	}

	if ( $pdata['count'] < $n*$n ) {

	$round = $pdata['round'];

	while ( $pdata['round'] == $round ) {
		$nn = $n - $pdata['round'] - 1;
		$ss = $pdata['round'];

		if (
			(
				( $pdata[0]['c'] < $nn
				&& $pdata[1]['c'] < $nn
				&& $pdata[0]['c'] < $pdata[1]['c']
				&& $pdata[0]['r'] == $pdata[1]['r'] )
				|| ( $pdata[0]['c'] < $ss
				&& $pdata[1]['c'] < $ss
				&& $pdata[0]['r'] > $pdata[1]['r']
				&& $pdata[1]['r'] == $ss )
			)
			&& ! isset( $data[ $pdata[1]['r'] ][ $pdata[1]['c'] + 1 ] )
		) {
			$r = $pdata[1]['r'];
			$c = $pdata[1]['c'] + 1;
		} elseif (
			(
				( $pdata[0]['c'] < $nn
				&& $pdata[1]['c'] == $nn
				&& $pdata[0]['c'] < $pdata[1]['c']
				&& $pdata[0]['r'] == $pdata[1]['r'] )
				|| ( $pdata[0]['c'] == $nn
				&& $pdata[1]['c'] == $nn
				&& $pdata[0]['r'] < $pdata[1]['r']
				&& $pdata[1]['r'] < $nn )
			)
			&& ! isset( $data[ $pdata[1]['r'] + 1 ][ $pdata[1]['c'] ] )
		) {
			$r = $pdata[1]['r'] + 1;
			$c = $pdata[1]['c'];
		} elseif (
			(
				( $pdata[0]['c'] == $nn
				&& $pdata[1]['c'] == $nn
				&& $pdata[0]['r'] < $pdata[1]['r']
				&& $pdata[1]['r'] == $nn )
				|| ( $pdata[0]['c'] <= $nn
				&& $pdata[1]['c'] > $ss
				&& $pdata[0]['c'] > $pdata[1]['c']
				&& $pdata[0]['r'] == $pdata[1]['r'] )
			)
			&& ! isset( $data[ $pdata[1]['r'] ][ $pdata[1]['c'] - 1 ] )
		) {
			$r = $pdata[1]['r'];
			$c = $pdata[1]['c'] - 1;
		} elseif (
			(
				( $pdata[0]['c'] < $nn
				&& $pdata[1]['c'] == $ss
				&& $pdata[0]['c'] > $pdata[1]['c']
				&& $pdata[0]['r'] == $pdata[1]['r'] )
				|| ( $pdata[0]['c'] == $ss
				&& $pdata[1]['c'] == $ss
				&& $pdata[0]['r'] > $pdata[1]['r']
				&& $pdata[1]['r'] > $ss )
			)
			&& ! isset( $data[ $pdata[1]['r'] - 1 ][ $pdata[1]['c'] ] )
		) {
			$r = $pdata[1]['r'] - 1;
			$c = $pdata[1]['c'];
		} else {
			$pdata['round'] += 1;
		}

		if ( $pdata['round'] == $round ) {
			$data[$r][$c] = $pdata[0]['v'] + $pdata[1]['v'];
			$pdata[0] = $pdata[1];
			$pdata[1] = array( 'r' => $r, 'c' => $c, 'v' => $data[$r][$c] );

			$pdata['count'] += 1;
		}

	}

	return stm_set_data ( $n, $data, $pdata );

	} else {

		$data = stm_sort_data( $data );

		return $data;

	}
}

function stm_sort_data( $data ) {

    $new_array = array();

    if ( count( $data ) > 0) {
        foreach ( $data as $arr ) {
            ksort( $arr );
            $new_array[] = $arr;
        }
    }

    return $new_array;
}
