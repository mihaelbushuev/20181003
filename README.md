Необходимо сделать небольшой сервис который генерирует двумерный массив по определенному алгоритму и возвращает ответ в виде HTML или JSON.

Сервис принимает запрос через GET с параметром N (целое положительное число. Параметр обязательный)

Часть 1 (HTML)
Страница с формой для задания необходимых параметров.
Результат в виде таблицы.

Часть 2 (JSON)
Если передать в GET дополнительно json=1, то результат выводится в виде json объекта с такими полями:
error boolean|string обязательный false если все хорошо, string с текстом ошибки если проблема
data array опциональный двумерный массив чисел, только если error=false
------------------------------ 
Описание алгоритма:
вход: N - ширина и высота, целое положительное 

выход:
массив A, размерностью N x N, заполненный по кругу слева на право и сверху вниз целыми числами, 
где значение каждого элемента равно сумме двух предыдущих. Но первые два элемента равны 0 и 1 соответственно.

обязательное условие:
необходимо использовать рекурсию при реализации алгоритма. ( т. е. один круг = одному вызову функции, которая потом вызывает себя для прохождения следующего внутреннего круга. И так до тех пор, пока не заполнится весь массив. )
------------------------------ 
пример:
N=3

Результат:
0 1 1
13 21 2
8 5 3
