			</div>    <!-- End container -->
		</section>

	</div>	<!-- END stm-page -->

	<!-- FOOTER
	============================================= -->
	<footer class="stm-footer full dark section-fit">
		<div class="stm-footer-bottom overlay">
			<div class="container">
				<div class="stm-footer-content row py-2">

					<div class="stm-footer-copy col-sm-12 text-center">
						&copy; <script type="text/javascript">
							var mdate = new Date(),
								ys = document.currentScript,
								ysp = ys.parentNode,
								y = document.createElement("span");
							y.innerHTML = mdate.getFullYear();
							ysp.insertBefore(y, ys);
						</script> Все права защищены
					</div>	<!-- END stm-footer-copy -->

				</div>	 <!-- End stm-footer-content -->
			</div>    <!-- End container -->
		</div>    <!-- End overlay -->
	</footer>	<!-- END stm-footer -->

	<!-- Jquery -->
	<script type="text/javascript" src="assets/components/jquery/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap -->
	<script type="text/javascript" src="assets/components/bootstrap/4.0.0-beta.3/js/vendor/popper.min.js"></script>
	<script type="text/javascript" src="assets/components/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>

	<!-- Functions -->
	<script type="text/javascript" src="assets/js/stm-page-size-functions.js"></script>

</body>
</html>
