<?php
		// Page Template
		include_once( 'input.php' );
?>

				<div class="row">
					<div class="col-md-5 mx-auto py-5">

						<form class="w-100" action="" method="get">

							<div class="col-12">

								<div class="mb-4">
									<h2 class="">Result</h2>
								</div>

<?php
	if ( ! is_array( $data ) ) {
?>
								<h3>Error: <?php echo $data; ?></h3>
<?php
	} else {
?>
								<table class="table">
									<tbody>
<?php
		foreach ( $data as $row ) {
?>
										<tr>
<?php
			foreach ( $row as $col ) {
?>
											<td><?php echo $col; ?></td>
<?php
			}
?>
										</tr>
<?php
		}
?>
									</tbody>
								</table>
<?php
	}
?>

							</div>

						</form>
					</div>
				</div>
