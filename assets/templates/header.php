<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="ru-RU">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="ru-RU">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="ru-RU">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="ru-RU">
<!--<![endif]-->
<head>

	<meta charset="UTF-8" />
	<meta http-equiv="Content-Language" content="ru" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Title -->
	<title>Mihael Bushuev - Test Task</title>

	<!-- Bootstrap -->
    <link rel='stylesheet' type='text/css' media='all' href='assets/components/tether/css/tether.min.css' />
	<link rel='stylesheet' type='text/css' media='all' href='assets/components/bootstrap/bootstrap.stm.css' />
	<link rel='stylesheet' type='text/css' media='all' href='assets/components/bootstrap/4.0.0-beta.3/css/bootstrap.min.css' />

	<!-- Fonts -->
	<link rel='stylesheet' type='text/css' media='all' href='assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css' />

	<!-- Google web fonts -->
	<link rel='stylesheet' type='text/css' media='all' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700,700italic'>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=cyrillic">

	<!-- Stylesheet -->
	<link rel="stylesheet" href="assets/css/styles.css" type="text/css" media="all">

</head>
<body>

	<div class="stm-page">

		<!-- INTRO
		============================================= -->
		<section class="stm-intro section-fit">
			<div class="overlay">
				<div class="container-fluid">

					<div class="stm-intro-caption row">
						<div class="mx-auto col-12 py-lg-1">
							<div class="stm-intro-content mx-auto text-center d-flex float-lg-left">

								<!-- Title 	-->
								<h3 class="text-700 text-center d-xs-inline-block mb-1 my-lg-2 mx-auto">
									Test Task
								</h3>

							</div>
							<div class="stm-nav-schedule mx-auto text-center text-lg-left float-lg-right">
								<i class="fa fa-user d-none d-lg-inline-block pr-2" aria-hidden="true"></i>
								<h3 class="text-700 text-center d-xs-inline-block mb-1 my-lg-2 mx-auto">
									Mihael Bushuev
								</h3>
							</div>
						</div>
					</div>	<!-- END stm-intro-caption -->

				</div>
			</div>
		</section>

		<!-- DATA
		============================================= -->
		<section class="stm-data section-fit-change">
			<div class="container">
