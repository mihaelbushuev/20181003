				<div class="row">
					<div class="col-md-5 mx-auto py-5">

						<form class="w-100" action="" method="get">

							<div class="col-12">

								<div class="mb-4">
									<h2 class="">Setting Parameters</h2>
								</div>

								<label for="dimension" class="sr-only">Array Dimension</label>
								<input id="dimension" name="n" class="form-control mb-3" placeholder="Array Dimension" min="0" step="1" pattern="^[0-9]" title="Only Positive Integer" oninput="validity.valid||(value='');" required="" type="number">

								<input id="send" name="send" class="btn btn-lg btn-primary btn-block" type="submit" value="Send">

							</div>

						</form>
					</div>
				</div>
