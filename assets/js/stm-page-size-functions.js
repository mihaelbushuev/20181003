// JavaScript Document
(function($) {

	function setPageSize(){
		var setpagesize = ( ( $('meta[property="stm:setpagesize"]').length == 0 ) ? 'true' : $('meta[property="stm:setpagesize"]').attr('content') );
//		console.log( 'setpagesize: ' + $('meta[property="stm:setpagesize"]').length );
		if( ( setpagesize == 'false' || setpagesize == '' ) || getPageSize().ww < 576 ) return;
			var height = getPageSize().wh;
//			console.log( 'height: ' + height );
			var ex_els = 'script, noscript';
			var sectionsfitheight = 0;
			var sectionsfitchange = $('.section-fit-change');
//			console.log( 'sectionsfitchange: ', sectionsfitchange );
			if( sectionsfitchange.length == 0 ){
				var sectionsfitchange = $('body > *:first-child:not("'+ex_els+'")');
			}
			$(sectionsfitchange).css('height','');
			$(sectionsfitchange).css('transition','height .5s');
			$('.section-fit').each(function(){
//				console.log( 'sectionsfitheight in: ' + sectionsfitheight );
//				console.log( '.section-fit - sectionsfitheight: ' + $(this).height(), $(this) );
//				console.log( 'sectionsfitheight new: ' + parseInt( sectionsfitheight + $(this).height() ) );
				sectionsfitheight = parseInt( sectionsfitheight + $(this).height() );
//				console.log( 'sectionsfitheight out: ' + sectionsfitheight );
			});
			if( sectionsfitheight == 0 ){
				$('body > *').not(ex_els).each(function(){
//					console.log( 'sectionsfitheight in: ' + sectionsfitheight );
//					console.log( 'body > * - sectionsfitheight: ' + $(this).height(), $(this) );
//					console.log( 'sectionsfitheight new: ' + parseInt( sectionsfitheight + $(this).height() ) );
					sectionsfitheight = parseInt( sectionsfitheight + $(this).height() );
//					console.log( 'sectionsfitheight out: ' + sectionsfitheight );
				});
			}
//			console.log( 'sectionsfitheight: ' + sectionsfitheight );
			var sectionsfitchangeheight = $(sectionsfitchange).height();
//			console.log( 'sectionsfitchangeheight: ' +sectionsfitchangeheight );
			if( height > ( sectionsfitheight + sectionsfitchangeheight ) ){
//				sectionsfitchangeheightnew = height - ( sectionsfitheight - sectionsfitchangeheight );
				sectionsfitchangeheightnew = height - sectionsfitheight;
				$(sectionsfitchange).height(sectionsfitchangeheightnew);
			}
//			console.log( 'sectionsfitchange 2: ', sectionsfitchange );
			setTimeout(function() {
				setPageSize();
			}, 1000);
	}

	$(document).ready(function() {
		"use strict";

//		console.log('setPageSize');
		addEvent(window,"load",setPageSize());

		$('.collapse').on('shown.bs.collapse hidden.bs.collapse', function () {
//			console.log('setPageSize');
			setPageSize();
		});

//		setInterval(function() {
//			setPageSize();
//			console.log('setPageSize');
//		}, 5000);

	});

	$(window).on('resize', function() {
		"use strict";
//		console.log('setPageSize');
		setPageSize();
	});

})(jQuery);

function addEvent(obj, evt, fn) {
	if (obj.addEventListener) {
		obj.addEventListener(evt, fn, false);
	}
	else if (obj.attachEvent) {
		obj.attachEvent("on" + evt, fn);
	}
}

function  getPageSize(){
	var xScroll, yScroll;

	if (window.innerHeight && window.scrollMaxY) {
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
		xScroll = document.documentElement.scrollWidth;
		yScroll = document.documentElement.scrollHeight;
	} else { // Explorer Mac...would also work in Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}

	var windowWidth, windowHeight;
	if (self.innerHeight) { // all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}

	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else {
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}

	return {
		pw: pageWidth,
		ph: pageHeight,
		ww: windowWidth,
		wh: windowHeight,
	}
}