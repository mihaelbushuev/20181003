<?php // You have to pay for everything. Not money, so time.

	// Initialization
	include_once( __DIR__ . '/init.php' );

	// Output
	if ( $cur_format == 'html' ) {

		// Header Template
		include_once( __DIR__ . '/assets/templates/header.php' );

		// Page Template
		include_once( __DIR__ . '/assets/templates/' . $cur_page . '.php' );

		// Footer Template
		include_once( __DIR__ . '/assets/templates/footer.php' );

	}
