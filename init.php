<?php // You have to pay for everything. Not money, so time.

	// Debug
	error_reporting( E_ALL );
	ini_set( 'log_errors', 1 );
	ini_set( 'display_errors', 0 );

	// Output format
	$cur_format = 'html';

	// Output format
	if ( isset( $_GET['json'] )	&& $_GET['json'] = '1' ) $cur_format = 'json';

	if (
		$cur_format != 'json'
		&& ( ! isset( $_GET['n'] ) )	// Dimension not sent
	) {

		// Data Input Template
		$cur_page = 'input';

	} else {

		// Result Output Template
		$cur_page = 'res';

		// Dimension
		$n = $_GET['n'];

		// Functions
		include_once( __DIR__ . '/functions/functions.php' );

		$data = stm_get_data ( $n, $cur_format );

	}
